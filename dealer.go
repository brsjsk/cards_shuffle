package main

func dealCards(d deck, handSize int) (deck, deck) {
	return d[:handSize], d[handSize:]
}
