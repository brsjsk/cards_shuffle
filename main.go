package main

import "fmt"

func main() {
	cards := newDeckFromFile("cards.txt")

	cards.shuffleCards()

	cards.print()
	lineDivider()

	hand, remainingCards := dealCards(cards, 3)
	hand.print()

	lineDivider()
	remainingCards.print()

	lineDivider()
	fmt.Println("Saving to file")
	cards.saveToFile("cards.txt")
}
