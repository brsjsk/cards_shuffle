package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

type deck []string

var cardSuits = []string{"Spades", "Diamonds", "Hearts", "Club"}
var cardValues = []string{"Ace", "Two", "Three", "Four"}

func newDeck() deck {
	cards := deck{}

	for _, suit := range cardSuits {
		for _, value := range cardValues {
			cards = append(cards, value+" of "+suit)
		}
	}

	return cards;
}

func (d deck) shuffleCards() {
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)
	for index := range d {
		randomNumber := r.Intn(len(d) - 1)

		d[index], d[randomNumber] = d[randomNumber], d[index]
	}
}

func (cards deck) print() {
	for i, card := range cards {
		fmt.Println(i, card)
	}
}

func (card deck) log(index int) {
	fmt.Println(card[index])
}

func (d deck) toString() string {
	return strings.Join(d, ",")
}

func (d deck) toByteString() []byte {
	return []byte(d.toString())
}

func (d deck) saveToFile(filename string) error {
	return ioutil.WriteFile(filename, d.toByteString(), 0666)
}

func newDeckFromFile(filename string) deck  {
	bs, err := ioutil.ReadFile(filename)

	if err != nil {
		fmt.Println("Error:", err);
		os.Exit(1);
	}

	sliceOfStrings := strings.Split(string(bs), ",")
	return deck(sliceOfStrings)
}