package main

import "fmt"

const LINE_DIVIDER string = "---------------------"

func lineDivider() {
	fmt.Println(LINE_DIVIDER)
}