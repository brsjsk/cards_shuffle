package main

import (
	"os"
	"testing"
)


func TestNewDeck(t *testing.T) {
	cards := newDeck();

	if (len(cards) != 16) {
		t.Errorf("Expected deck length of 16")
	}

	if (cards[0] != "Ace of Spades") {
		t.Errorf("Expected Ace of Spades to be first")
	}

	if (cards[len(cards) - 1] != "Four of Club") {
		t.Errorf("Expected Four of Club to be last")
	}
}

func TestSaveToDeckAndNewDeckFromFile(t *testing.T) {
	os.Remove("_decktestingtmp")

	deck := newDeck()

	deck.saveToFile("_decktestingtmp")

	loadedDeck := newDeckFromFile("_decktestingtmp")

	if (len(loadedDeck) != 16) {
		t.Errorf("Expected 16 cards in loaded deck")
	}

	os.Remove("_decktestingtmp")
}